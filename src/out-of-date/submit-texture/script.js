var selectedFiles;

if (!Array.prototype.indexOf) { // For IE < 9
  Array.prototype.indexOf = function(elt /*, from*/) {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++) {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

String.prototype.contains = function(substring) {
  return ~this.indexOf(substring);
};

$(document).ready(function() {
  if (DragAndDrop.isSupported()) {
    var handleFiles = function(files) {
      selectedFiles = files;
      if (files) $("#status-text").text("File selected.");
    };

    DragAndDrop.addDropTarget($("#content"), handleFiles);
    DragAndDrop.createBrowseButton($("#choose-file-button"), $("#fileinput"), handleFiles);
  } else {
    $("#drop").text("Some required features are missing from your browser. :(");
  }
});

function submitTexture() {
  var username = $("#in-name").val();
  $("#in-name").removeClass("error");
  $("#drop").removeClass("error");

  if (username.length == 0) {
    $("#in-name").addClass("error");
    setTimeout(function() {$("#in-name").removeClass("error");}, 750);
    $("#status-text").text("No username set.");
    return;
  }

  if (username.contains("/")) {
    var parts = username.split("/");
    username = parts[parts.length - 1];
  }
  if (username.contains("\\")) {
    var parts = username.split("\\");
    username = parts[parts.length - 1];
  }
  if (username.length > 20) {
    username = username.substring(0, 20);
  }
  username = username.replace(".", "_");

  if (username.length == 0) {
    $("#in-name").addClass("error");
    setTimeout(function() {$("#in-name").removeClass("error");}, 750);
    $("#status-text").text("Invalid username.");
    return;
  }

  if (selectedFiles && selectedFiles.length > 0) {
    if (selectedFiles[0].size <= 5242880) {
      var reader = new FileReader();
      reader.onload = function(event) {
        $.ajax({
          url: "/dropbox/upload?arg=" + JSON.stringify({ path: "/Submitted Textures/" + username + "_" + new Date().getTime() + ".png" }),
          type: 'POST',
          data: new Blob([reader.result], { type: 'application/octet-stream' }),
          cache: false,
          contentType: false,
          processData: false,
          success: function(data) {
            $("#status-text").text("Upload successfull.");
          },
          error: function() {
            $("#status-text").text("Upload failed.");
          }
        });
      };
      reader.onerror = function(event) {
        $("#status-text").text("Could not read the file.");
      };
      $("#status-text").text("Uploading...");
      reader.readAsArrayBuffer(selectedFiles[0]);
    } else {
      $("#status-text").text("This file is too big. The size limit is 5 MB.");
    }
    selectedFiles = null;
  } else {
    $("#drop").addClass("error");
    setTimeout(function() {$("#drop").removeClass("error");}, 750);
    $("#status-text").text("No files selected.");
  }
}