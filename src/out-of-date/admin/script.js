var jsonData = {};
var packLoaded = -1;
var oldPackName = "";

function updateContentAreaWidth() {
  if (window.innerWidth < 1280) {
    $("#content").css("width", window.innerWidth.toString() + "px");
  } else {
    $("#content").css("width", "1280px");
  }
}

function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

function updatePackList() {
  $("#pack-list").empty();
  for (var i = 0; i < jsonData.packs.length; i++) {
    addPack(i);
  };
}

function sortList() {
  if (packLoaded != -1) savePack();
  $("#editor").empty();
  $("#delete-button").css("display", "none");
  packLoaded = -1;

  // Do not sort resource packs; Light, High and Dark needs
  // to stay in their order at the top and Dwarven just below.
  var resourcePacks = [];
  var others = [];
  for (var i = 0; i < jsonData.packs.length; i++) {
    if (jsonData.packs[i].category.endsWith("Resource Pack")) {
      resourcePacks.push(jsonData.packs[i]);
    } else {
      others.push(jsonData.packs[i]);
    }
  };

  // Sort non-resource packs alphabetically.
  others.sort(function(a, b) {return a.name.localeCompare(b.name)});

  jsonData.packs = resourcePacks.concat(others);
  updatePackList();
}

function saveJson() {
  sortList();
  download("packs.json", JSON.stringify(jsonData, null, 2));
}

function deletePack() {
  if (confirm("Are you sure you want to delete this pack?")) {
    $("#editor").empty();
    jsonData.packs.splice(packLoaded, 1);
    $("#delete-button").css("display", "none");
    packLoaded = -1;
    updatePackList();
  }
}

function updatePackName() {
  if ($(".in-name").val() != oldPackName) {
    var packButton = $("#pack-list>a").eq(packLoaded);
    packButton.text($(".in-name").val() == "" ? "- Unnamed Pack -" : $(".in-name").val());
  }
}

function savePack() {
  updatePackName();
  jsonData.packs[packLoaded] = {
    name: $(".in-name").val(),
    category: $(".in-category").val(),
    preview: $(".in-preview").val(),
    page: $(".in-page").val(),
    screenshots: []
  }
  if ($(".in-mod-page").val() != "") {
    jsonData.packs[packLoaded].modPage = $(".in-mod-page").val();
  }
  if ($(".in-screenshots").val() != "") {
    jsonData.packs[packLoaded].screenshots = $(".in-screenshots").val().split(",");
  }
  if ($(".in-enable-panorama").val().toLowerCase() == "true") {
    jsonData.packs[packLoaded].enablePanorama = true;
  }
  jsonData.packs[packLoaded].downloads = [];
  var dls = $(".downloadable");
  for (var i = 0; i < dls.length; i++) {
    var dl = dls.eq(i);
    jsonData.packs[packLoaded].downloads[i] = {
      version: dl.find(".in-version").val()
    }
    if (dl.find(".in-direct").val() != "") {
      jsonData.packs[packLoaded].downloads[i].direct = dl.find(".in-direct").val();
    }
    if (dl.find(".in-mirror").val() != "") {
      jsonData.packs[packLoaded].downloads[i].mirror = dl.find(".in-mirror").val();
    }
  };
}

function removeDownload(i) {
  $(".downloadable:nth-child(" + i + ")").remove();
}

function addDownload(pack, i) {
  pack = pack || {"downloads":[]};
  i = (i === 0) ? i : i || Infinity;
  var dl = pack.downloads.length > i ? pack.downloads[i] : {};
  var dlDiv = $("<div></div>").addClass("downloadable");
  var count = $(".downloads-container").length;
  dlDiv.append($("<h3></h3>").text("Version:").append($("<p></p>").addClass("remove-button").click(function() {removeDownload(count + 1)})));
  if (dl.hasOwnProperty("version")) {
    dlDiv.append($("<input></input>").attr("type", "text").addClass("in-version").val(dl.version));
  } else {
    dlDiv.append($("<input></input>").attr("type", "text").addClass("in-version"));
  }
  dlDiv.append($("<h3></h3>").text("Direct link:").append($("<i>Preferably from the <a href=\"https://bitbucket.org/DokucraftSaga/dokucraftsaga.bitbucket.org/downloads\" target=\"_blank\">download section</a>.</i>")));
  if (dl.hasOwnProperty("direct")) {
    dlDiv.append($("<input></input>").attr("type", "text").addClass("in-direct").val(dl.direct));
  } else {
    dlDiv.append($("<input></input>").attr("type", "text").addClass("in-direct"));
  }
  dlDiv.append($("<h3></h3>").text("Mirror:").append($("<i>Preferably from Mediafire.</i>")));
  if (dl.hasOwnProperty("mirror")) {
    dlDiv.append($("<input></input>").attr("type", "text").addClass("in-mirror").val(dl.mirror));
  } else {
    dlDiv.append($("<input></input>").attr("type", "text").addClass("in-mirror"));
  }
  $(".downloads-container").append(dlDiv);
  updateContentAreaWidth();
}

function loadEditor(i) {
  if (packLoaded != -1) savePack();
  var pack = jsonData.packs[i];
  packLoaded = i;
  $("#delete-button").css("display", "block");
  oldPackName = pack.name;
  $("#editor").empty().append(
    $("<h3></h3>").text("Name:")
  ).append(
    $("<input></input>").attr("type", "text").addClass("in-name").val(pack.name).on("change", updatePackName)
  ).append(
    $("<h3></h3>").text("Category:")
  ).append(
    $("<input></input>").attr("type", "text").addClass("in-category").val(pack.category)
  ).append(
    $("<h3></h3>").text("Preview:")
  ).append(
    $("<input></input>").attr("type", "text").addClass("in-preview").val(pack.preview)
  ).append(
    $("<h3></h3>").text("Page:")
  ).append(
    $("<input></input>").attr("type", "text").addClass("in-page").val(pack.page)
  ).append(
    $("<h3></h3>").text("Mod page:").append($("<i>Only for Mod Support packs.</i>"))
  ).append(
    $("<input></input>").attr("type", "text").addClass("in-mod-page").val(pack.modPage)
  ).append(
    $("<h3></h3>").text("Screenshots:").append($("<i>Separate multiple screenshots with a comma. E.g.: file1.png,file2.png,file3.png</i>"))
  ).append(
    $("<input></input>").attr("type", "text").addClass("in-screenshots").val(pack.screenshots.join(","))
  ).append(
    $("<h3></h3>").text("Enable panorama preview")
  ).append(
    $("<input></input>").attr("type", "text").addClass("in-enable-panorama").val(pack.enablePanorama ? "true" : "false")
  ).append(
    $("<h3></h3>").text("Downloads:")
  ).append(
    $("<div></div>").addClass("downloads-container")
  ).append(
    $("<a>+ Add download</a>").addClass("add-download-button").click(function() {addDownload();})
  );
  for (var i = 0; i < pack.downloads.length; i++) {
    addDownload(pack, i);
  };
  updateContentAreaWidth();
}

function addPack(i) {
  var pack = jsonData.packs[i];
  $("#pack-list").append($("<a></a>").text(pack.name).click(function() {loadEditor(i);}));
}

function addNewPack() {
  var idx = jsonData.packs.length;
  jsonData.packs.push({
    name: "- New Pack -",
    screenshots: [],
    downloads: []
  });
  addPack(idx);
}

$(window).resize(updateContentAreaWidth);
$(document).ready(function() {
  updateContentAreaWidth();
  $.getJSON("/packs.json", function(data) {
    jsonData = data;
    updatePackList();
  });
});